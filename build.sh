#!/bin/bash

# Creates necessary empty directories for volume mapping
if [ ! -d "$PWD/config/ssl" ]; then
    mkdir -p "$PWD/config/ssl"
fi

if [ ! -d "$PWD/www" ]; then
    mkdir -p "$PWD/www"
fi

if [ ! -d "$PWD/data/mysql" ]; then
    mkdir -p "$PWD/data/mysql"
fi

if [ ! -d "$PWD/logs/apache2" ] || [ ! -d "$PWD/logs/mysql" ] || [ ! -d "$PWD/logs/xdebug" ]; then
    mkdir -p "$PWD/logs/apache2"
    mkdir -p "$PWD/logs/mysql"
    mkdir -p "$PWD/logs/xdebug"
fi

docker-compose --env-file "./www/.env" up -d
docker exec -w "/var/www/html/www" it-management-php81 composer install
docker exec -w "/var/www/html/www" it-management-php81 ./vendor/bin/phinx migrate
