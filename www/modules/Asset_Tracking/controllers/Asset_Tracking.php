<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/app.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/authentication/authentication.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/authentication/roles.php');
require_once($_SERVER['DOCUMENT_ROOT'] . "/mysql/config.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/php/config.php");

$app = $_SERVER['DOCUMENT_ROOT'] . '/modules/Asset_Tracking/app.json';

$title = getValue($app, 'module_name');

$response = prepareNotification($_GET);


// If the user is not logged in, redirect to login view
if (!isLogged()) {
    header('Location: /Login');
}

function getAssets($conn)
{
    $query = "SELECT * FROM `Asset_Tracking`";

    $res = mysqli_query($conn, $query);
    $assets = [];
    while ($row = mysqli_fetch_object($res)) {
        $assets[] = $row;
    }
    // print_r($assets);
    return $assets;
}

function add()
{
    echo 'add function';
}

echo $_SESSION['TWIG']->render(getValue($app, 'view_path'), [
    'title' => $title, //Expected by the header
    'userName' => $_SESSION['current_user']['firstName'], //Expected for nav bar user's name display
    'userView' => checkPrivilege('view_users', $_SESSION['user_roles']), //Expected for nav bar to show (or not) the users table view
    'rolesView' => checkPrivilege('view_roles', $_SESSION['user_roles']),
    'appName' => $_ENV['APP_NAME'], //Expected for nav bar to show name of the application
    'modules' => $_SERVER['MODULE_PATHS'], //Expected side navbar
    'response' => json_encode($response),
    'assets' => getAssets($conn)
]);
